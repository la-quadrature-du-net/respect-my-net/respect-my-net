# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-05-23 18:12+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: settings.py:47
msgid "French"
msgstr ""

#: settings.py:48
msgid "English"
msgstr ""

#: settings.py:49
msgid "Spanish"
msgstr ""

#: settings.py:50
msgid "Italian"
msgstr ""

#: templates/base.html:39
msgid "Report cases of Net Neutrality violations"
msgstr ""

#: templates/base.html:46
msgid "Toggle navgation"
msgstr ""

#: templates/base.html:56
msgid "Cases"
msgstr ""

#: templates/base.html:57
msgid "What is Net Neutrality?"
msgstr ""

#: templates/base.html:58
msgid "Start Monitoring"
msgstr ""

#: templates/base.html:64
msgid "Admin Action"
msgstr ""

#: templates/base.html:66
msgid "Logout"
msgstr ""

#: templates/base.html:67
msgid "Admin"
msgstr ""

#: templates/base.html:73
msgid "Login"
msgstr ""

#: templates/base.html:112 templates/list.html:84
#: templates/search/search.html:104
msgid "Close"
msgstr ""

#: templates/base.html:136
msgid ""
"RespectMyNet.eu is an online platform enabling citizens to report net "
"neutrality violations. Everyone is invited to report blocking or throttling "
"of their Internet access or unfair discrimination between online services. "
"This will help to shed light on those harmful practices and provide evidence "
"to the telecom regulators to do their duty to defend the open and innovative "
"Internet in Europe."
msgstr ""

#: templates/comments/form.html:19
msgid "Post"
msgstr ""

#: templates/comments/form.html:20
msgid "Preview"
msgstr ""

#: templates/feeditem.html:4 templates/view.html:62
msgid "Description"
msgstr ""

#: templates/feeditem.html:10 templates/view.html:67
msgid "Attachments"
msgstr ""

#: templates/feeditem.html:21 templates/view.html:45
msgid "Media"
msgstr ""

#: templates/feeditem.html:22
msgid "Affected Resource/service"
msgstr ""

#: templates/feeditem.html:23 templates/view.html:44
msgid "Type"
msgstr ""

#: templates/feeditem.html:24 templates/view.html:47
msgid "Temporary restriction"
msgstr ""

#: templates/feeditem.html:24 templates/feeditem.html.py:26
#: templates/view.html:47 templates/view.html.py:48 templates/view.html:49
msgid "yes"
msgstr ""

#: templates/feeditem.html:25
msgid "Loophole offering"
msgstr ""

#: templates/feeditem.html:26 templates/view.html:49
msgid "Contractual restriction"
msgstr ""

#: templates/feeditem.html:28 templates/view.html:50
msgid "Contract excerpt"
msgstr ""

#: templates/index.html:62 templates/index.html.py:146 templates/list.html:70
#: templates/search/search.html:90
msgid "Me too!"
msgstr ""

#: templates/index.html:66 templates/index.html.py:133 templates/list.html:77
#: templates/search/search.html:97 templates/view.html:90
msgid ""
"In order to <strong>confirm</strong> this report, please enter your email "
"address"
msgstr ""

#: templates/index.html:69
msgid "Ok"
msgstr ""

#: templates/index.html:99
msgid ""
"On the Internet, <strong>you</strong> decide what you do, right? <i>Maybe "
"not!</i>"
msgstr ""

#: templates/index.html:100
msgid ""
"Telecommunication providers and Internet companies are trying to control "
"what you do online. They want to block and throttle some of your "
"communications, and charge you to use certain online services, content or "
"applications. The main aim of RespectMyNet is to bring attention to these "
"Net Neutrality violations and demand that regulators take action to stop "
"this. "
msgstr ""

#: templates/index.html:101
msgid ""
"The EU law recently adopted on Net Neutrality is ambiguous and the telecom "
"regulators have the job of deciding how it should be implemented."
msgstr ""

#: templates/index.html:102
msgid ""
"You can help. You can report Net Neutrality violations and help the "
"regulators protect the open Internet in Europe!"
msgstr ""

#: templates/index.html:103
msgid ""
"Help us keep track of all the ways that Internet providers are violating our "
"online freedoms. Tell us about how your landline or mobile Internet operator "
"restricts your Internet connection. It will take less than two minutes!"
msgstr ""

#: templates/index.html:104
msgid ""
"Don't hesitate to give us feedback on this application by reporting any idea "
"or issue with RespectMyNet.eu here: <a href='https://nnmon.quadpad.lqdn.fr/"
"feedback'>Feedback pad page</a>"
msgstr ""

#: templates/index.html:105 templates/search/search.html:52
msgid "Statistics"
msgstr ""

#: templates/index.html:117
msgid "Featured Cases"
msgstr ""

#: templates/index.html:121
msgid "Infringing offering"
msgstr ""

#: templates/index.html:122
msgid "Confirmed"
msgstr ""

#: templates/index.html:138 templates/list.html:83
#: templates/search/search.html:103 templates/view.html:96
msgid "OK"
msgstr ""

#: templates/index.html:139 templates/view.html:97
msgid "Cancel"
msgstr ""

#: templates/index.html:152
msgid "Affected Resource"
msgstr ""

#: templates/index.html:167
msgid "Add New Case"
msgstr ""

#: templates/index.html:180
msgid ""
"Please report cases of zero-rating (services which are excluded from your "
"download limit), specialised services or the blocking, throttling or "
"prioritisation of online services. For a more detailed description of what "
"to report, check our <a href='/about/#guidelines'>guidelines</a>."
msgstr ""

#: templates/index.html:205
msgid "save"
msgstr ""

#: templates/index.html:212
msgid "Continue"
msgstr ""

#: templates/index.html:223
msgid "Similar cases"
msgstr ""

#: templates/index.html:226
msgid ""
"Those are cases that might be similar to yours, please feel free to add your "
"input on those cases instead of adding yours if they overlap."
msgstr ""

#: templates/index.html:232
msgid "Dismiss"
msgstr ""

#: templates/list.html:38
msgid "Reported Cases"
msgstr ""

#: templates/list.html:40
msgid ""
"This is a list of all reported cases of Net Neutrality violations by our "
"users. Please note that we do not validate the accuracy of these reports "
"before they are published on this list, but rather rely on confirmations and "
"supporting evidence offered by users. If you are subjected to one of the "
"listed restrictions, please confirm it. Cases that are not considered "
"violations of Net Neutrality under our guidelines will be removed or not be "
"validated."
msgstr ""

#: templates/list.html:47 templates/search/search.html:66
#: templates/view.html:46
msgid "Status"
msgstr ""

#: templates/list.html:48 templates/search/search.html:67
msgid "country"
msgstr ""

#: templates/list.html:49 templates/search/search.html:68
msgid "operator"
msgstr ""

#: templates/list.html:50 templates/search/search.html:69
msgid "contract"
msgstr ""

#: templates/list.html:51 templates/search/search.html:70
msgid "resource"
msgstr ""

#: templates/list.html:52 templates/search/search.html:71
msgid "type"
msgstr ""

#: templates/list.html:53 templates/search/search.html:72
msgid "fixed / wireless"
msgstr ""

#: templates/list.html:54 templates/search/search.html:73
msgid "confirmations"
msgstr ""

#: templates/list.html:61 templates/search/search.html:81
msgid "New"
msgstr ""

#: templates/nn.html:7
msgid ""
"On the Internet, <strong>you</strong> decide what you do, right? <em>Maybe "
"not!</em>"
msgstr ""

#: templates/nn.html:8
msgid ""
"Online companies and telecommunication providers want to control what you do "
"online. For example, they want to restrict access to Internet telephony in "
"order to force you to use the telephone service that they want you to use. "
"They want to charge you extra for watching videos or listening to music, if "
"you choose a service that they are not promoting. They want to prohibit the "
"use of specific software on their networks or throttle innovative "
"applications such as peer-to-peer filesharing. They want people to pay to "
"access your blog."
msgstr ""

#: templates/nn.html:9
msgid ""
"This must stop. We want to ensure that your rights and freedoms online are "
"protected and that you decide which content you access and which "
"applications and services you use."
msgstr ""

#: templates/nn.html:10
msgid ""
"Net Neutrality violations harm freedom of expression, freedom of "
"information, freedom of choice, innovation, competition, privacy and "
"increase communication costs. "
msgstr ""

#: templates/nn.html:11
msgid ""
"We need to ensure that the Internet remains free, open and accessible for "
"all. This is what net neutrality is about. The European Commission and "
"national regulators need to prohibit providers from restricting your online "
"traffic and do so before it is too late. In order to convince them about the "
"urgency of this problem, we aim to create a comprehensive data set of Net "
"Neutrality violations in Europe. "
msgstr ""

#: templates/nn.html:12
msgid "Guidelines for reporting cases"
msgstr ""

#: templates/nn.html:13
msgid ""
"Internet providers can abuse their control over their network in several "
"ways. For example, they can discriminate between individual applications by "
"throttling, blocking or prioritising them - this is called &ldquo;technical "
"discrimination&rdquo;. They can also discriminate between individual "
"applications by excluding them from your monthly data cap - this is called "
"&ldquo;economic discrimination&rdquo;, which is commonly known as &ldquo;"
"zero-rating&rdquo;. A mixture of both types of discrimination is usually "
"encountered in the provision of abusive &ldquo;Specialised Services&rdquo;, "
"which can turn into paid-fast lanes."
msgstr ""

#: templates/nn.html:14
msgid ""
"When reporting economic net neutrality violations through RespectMyNet.eu, "
"please provide us with a description of the practice of your ISP. We need to "
"know what services are not counted towards your monthly data cap and what "
"happens with your Internet connection if your data cap is exceeded. By "
"providing links to the relevant offerings, advertisement material and terms-"
"of-service of the product, you are providing us with crucial evidence to "
"present to the regulators. If you are not sure on how to classify a "
"violation that you encounter, just leave the relevant fields empty. "
"Submissions can be discussed and modified once they are in the system."
msgstr ""

#: templates/nn.html:15
msgid ""
"When reporting technical net neutrality violations through RespectMyNet.eu, "
"please describe only connection issues that are related to traffic "
"discrimination, that is to say cases where Internet access providers "
"discriminate between traffic according to the source, destination, type or "
"actual content of the data transmitted over the network (i.e. if your "
"provider blocks traffic coming from YouTube or slows down Usenet traffic). "
"If possible, please verify whether the provider is the one to be blamed: "
"sometimes it is just the server itself which is not working (i.e. if a "
"website is down, this is obviously not the fault of the Internet access "
"provider)."
msgstr ""

#: templates/nn.html:16
msgid ""
"Sample of common ISP issues that DO NOT require a formal complaint (from: <a "
"href='http://www.ispreview.co.uk/new/complain/complain.shtml'>http://www."
"ispreview.co.uk/new/complain/complain.shtml</a>)"
msgstr ""

#: templates/nn.html:19
msgid ""
"<b>Brief and Uncommon Critical Service (Email, Website Browsing etc.) "
"Outages.</b>"
msgstr ""

#: templates/nn.html:20
msgid ""
"Unfortunately, ISPs experience occasional problems with online services, "
"such as email access. These are usually resolved after a few minutes or "
"hours, and only very occasionally will they last longer than a day. Don't "
"get too frustrated, inform them of your problem and allow some time for it "
"to be resolved."
msgstr ""

#: templates/nn.html:23
msgid "<b>General Fluctuations in Broadband Speed.</b>"
msgstr ""

#: templates/nn.html:24
msgid ""
"Broadband is a so-called &ldquo;best effort&rdquo; service, which means it "
"is shared between many users and open to fluctuations in performance "
"(especially in dense urban areas). For example, if you have an 8Mbps package "
"but your line is only rated to cope with 2Mbps, you should not be surprised "
"to see speeds vary from around 1 to 1.5Mbps (or even lower during peak "
"periods). Very few providers provide information about broadband speed. This "
"should change after the entry in operation of the new European net "
"neutrality rules. Your ISP has to provide you with information about the "
"traffic management that affects you, as well as included details in your "
"contract regarding the minimum, maximum and average bandwidth speed of your "
"Internet connection."
msgstr ""

#: templates/nn.html:27
msgid "<b>Short and Uncommon Broadband Connection Problems.</b>"
msgstr ""

#: templates/nn.html:28
msgid ""
"Once again, connections do sometimes go down and fail to connect/reconnect, "
"though usually for no more than one day at most. These issues should be very "
"rare and are usually fixed within a few minutes or hours."
msgstr ""

#: templates/registration/activate.html:8
msgid "Your account activation might work now. If not mail me."
msgstr ""

#: templates/registration/activation_complete.html:8
msgid "Your account is now active."
msgstr ""

#: templates/registration/activation_email.txt:4
msgid "Here's your activation key"
msgstr ""

#: templates/registration/activation_email.txt:7
msgid "Please click on this link to activate your account"
msgstr ""

#: templates/registration/activation_email.txt:10
msgid "This link will expire in:"
msgstr ""

#: templates/registration/activation_email.txt:10
msgid "days."
msgstr ""

#: templates/registration/login.html:13
#: templates/registration/password_reset_confirm.html:4
#: templates/registration/password_reset_form.html:4
msgid "Password reset"
msgstr ""

#: templates/registration/password_change_done.html:4
msgid "Password change successful"
msgstr ""

#: templates/registration/password_change_done.html:8
msgid "Your password was changed."
msgstr ""

#: templates/registration/password_change_form.html:4
msgid "Password change"
msgstr ""

#: templates/registration/password_change_form.html:12
msgid "Please correct the error below."
msgid_plural "Please correct the errors below."
msgstr[0] ""
msgstr[1] ""

#: templates/registration/password_change_form.html:16
msgid ""
"Please enter your old password, for security's sake, and then enter your new "
"password twice so we can verify you typed it in correctly."
msgstr ""

#: templates/registration/password_change_form.html:22
msgid "Old password"
msgstr ""

#: templates/registration/password_change_form.html:27
msgid "New password"
msgstr ""

#: templates/registration/password_change_form.html:32
msgid "Password (again)"
msgstr ""

#: templates/registration/password_change_form.html:38
#: templates/registration/password_reset_confirm.html:19
msgid "Change my password"
msgstr ""

#: templates/registration/password_reset_complete.html:4
msgid "Password reset complete"
msgstr ""

#: templates/registration/password_reset_complete.html:8
msgid "Your password has been set.  You may go ahead and log in now."
msgstr ""

#: templates/registration/password_reset_complete.html:10
msgid "Log in"
msgstr ""

#: templates/registration/password_reset_confirm.html:10
msgid "Enter new password"
msgstr ""

#: templates/registration/password_reset_confirm.html:12
msgid ""
"Please enter your new password twice so we can verify you typed it in "
"correctly."
msgstr ""

#: templates/registration/password_reset_confirm.html:16
msgid "New password:"
msgstr ""

#: templates/registration/password_reset_confirm.html:18
msgid "Confirm password:"
msgstr ""

#: templates/registration/password_reset_confirm.html:24
msgid "Password reset unsuccessful"
msgstr ""

#: templates/registration/password_reset_confirm.html:26
msgid ""
"The password reset link was invalid, possibly because it has already been "
"used.  Please request a new password reset."
msgstr ""

#: templates/registration/password_reset_done.html:4
msgid "Password reset successful"
msgstr ""

#: templates/registration/password_reset_done.html:8
msgid ""
"We've e-mailed you instructions for setting your password to the e-mail "
"address you submitted. You should be receiving it shortly."
msgstr ""

#: templates/registration/password_reset_email.html:2
msgid "You're receiving this e-mail because you requested a password reset"
msgstr ""

#: templates/registration/password_reset_email.html:3
#, python-format
msgid "for your user account at %(site_name)s"
msgstr ""

#: templates/registration/password_reset_email.html:5
msgid "Please go to the following page and choose a new password:"
msgstr ""

#: templates/registration/password_reset_email.html:9
msgid "Your username, in case you've forgotten:"
msgstr ""

#: templates/registration/password_reset_email.html:11
msgid "Thanks for using our site!"
msgstr ""

#: templates/registration/password_reset_email.html:13
#, python-format
msgid "The %(site_name)s team"
msgstr ""

#: templates/registration/password_reset_form.html:8
msgid ""
"Forgotten your password? Enter your e-mail address below, and we'll e-mail "
"instructions for setting a new one."
msgstr ""

#: templates/registration/password_reset_form.html:12
msgid "E-mail address:"
msgstr ""

#: templates/registration/password_reset_form.html:12
msgid "Reset my password"
msgstr ""

#: templates/registration/registration_complete.html:8
msgid "An email containing account-activation information has been sent."
msgstr ""

#: templates/search/search.html:41
msgid "Search through cases"
msgstr ""

#: templates/search/search.html:45
msgid "Save"
msgstr ""

#: templates/start.html:7
msgid ""
"Do you want to check whether your access provider is manipulating your "
"internet traffic? Great!"
msgstr ""

#: templates/start.html:8
msgid ""
"There are a lot of tools which make life easier for you. You can find very "
"much more information about this subject on the website of "
msgstr ""

#: templates/start.html:8
msgid "Here is a short selection."
msgstr ""

#: templates/start.html:9
msgid "Recommended tools"
msgstr ""

#: templates/start.html:10
msgid ""
"These tools are probably the most helpful when it comes to tracking traffic "
"discrimination."
msgstr ""

#: templates/start.html:13
msgid "<em>Available for Mac OS X, Ubuntu (GNU/Linux) and Windows.</em>"
msgstr ""

#: templates/start.html:14
msgid ""
"Neubot is a research project on network neutrality of the NEXA Center for "
"Internet & Society at Politecnico di Torino. The project is based on a "
"lightweight open-source programme that interested users can download and "
"install on their computers. The programme runs in background and "
"periodically performs transmission tests with some test servers and with "
"other instances of the programme itself. These transmission tests probe the "
"Internet using various application level protocols. The program saves tests "
"results locally and uploads them on the project servers. The collected "
"dataset contains samples from various Providers and allows to monitor "
"network neutrality."
msgstr ""

#: templates/start.html:17
msgid "<em>In-browser Java.</em>"
msgstr ""

#: templates/start.html:18
msgid ""
"Test whether certain applications or traffic are being blocked or throttled "
"on your broadband connection.  Glasnost attempts to detect whether your "
"Internet access provider is performing application-specific traffic shaping. "
"Currently, you can test if your ISP is throttling or blocking email, HTTP or "
"SSH transfer, Flash video, and P2P apps including BitTorrent, eMule and "
"Gnutella."
msgstr ""

#: templates/start.html:22
msgid "Additional tools"
msgstr ""

#: templates/start.html:23
msgid ""
"These tools are interesting to test the overall quality of your Internet "
"connections."
msgstr ""

#: templates/start.html:26
msgid ""
"<em>Runs on any browser and on <a href='http://ndt.googlecode.com/files/"
"AndroidNdt-1.0b1.apk'>Android</a>.</em>"
msgstr ""

#: templates/start.html:27
msgid ""
"Test your connection speed and receive sophisticated diagnosis of problems "
"limiting speed."
msgstr ""

#: templates/start.html:30
msgid "Diagnose common problems that impact last-mile broadband networks."
msgstr ""

#: templates/start.html:33 templates/start.html.py:37
msgid "<em>Runs on Mac OS X, GNU/Linux and Windows</em>"
msgstr ""

#: templates/start.html:34
msgid "See how much bandwidth your connection provides"
msgstr ""

#: templates/start.html:38
msgid ""
"Determine whether an ISP is performing traffic shaping. ShaperProbe detects "
"whether your ISP performs \"traffic shaping\". Traffic shaping means that "
"your ISP automatically drops your access rate after you have downloaded or "
"uploaded a certain number of bytes. ShaperProbe detects whether traffic "
"shaping is used in either the upload or download directions, and in that "
"case that it is used, ShaperProbe reports the shaping rate and the \"maximum "
"burst size\" before shaping begins."
msgstr ""

#: templates/start.html:41
msgid ""
"Torrent-protocol based sensor plugin for Vuze/Azureus BitTorrent client "
"using a crowd approach sharing data with peers."
msgstr ""

#: templates/start.html:44
msgid ""
"Torrent-protocol based sensor plugin for Vuze/Azureus BitTorrent client."
msgstr ""

#: templates/start.html:47
msgid "browser java applet"
msgstr ""

#: templates/start.html:48
msgid ""
"comprehensive tcp/udp and protocol level sensor see <a href=\"http://"
"netalyzr.icsi.berkeley.edu/restore/id=example-session\">example report</a> "
"for capabilities"
msgstr ""

#: templates/view.html:9
msgid "Edit"
msgstr ""

#: templates/view.html:37
msgid "This case was verified by the management team"
msgstr ""

#: templates/view.html:38
msgid "Nobody has yet confirmed the case. Feel free to do it"
msgstr ""

#: templates/view.html:38
msgid "person has confirmed this case"
msgstr ""

#: templates/view.html:38
msgid "people have confirmed this case"
msgstr ""

#: templates/view.html:43
msgid "Affected resource"
msgstr ""

#: templates/view.html:48
msgid "Another offer provided by the same operator removes this restriction"
msgstr ""

#: templates/view.html:56
msgid "RespectMyNet note"
msgstr ""

#: templates/view.html:84
msgid "This restriction affects me too"
msgstr ""

#: templates/view.html:105
msgid "Comments"
msgstr ""

#: templates/view.html:125
msgid "Approve Submission"
msgstr ""

#: templates/view.html:126
msgid "Delete Submission"
msgstr ""
