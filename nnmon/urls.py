from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView, RedirectView
from django.conf import settings
from django.contrib import admin
from bt import views as bt
from bt.feeds import RssSiteNewsFeed, AtomSiteNewsFeed

from bt.api import APIResource, OperatorResource

api_resource = APIResource()
operator_api_resource = OperatorResource()

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^$',
                           view=bt.AddForm.as_view(),
                           name="homepage"),
                       url(r'^list/$',
                           view=bt.ViolationsList.as_view(),
                           name="list_violations"),
                       url(r'^list/(?P<country>\w+)/$',
                           view=bt.ViolationsList.as_view(),
                           name="filter_country"),
                       url(r'^list/(?P<country>\w+)/(?P<operator>\w+)/$',
                           view=bt.ViolationsList.as_view(),
                           name="filter_operator"),
                       # violation cannonical url and redirections
                       url(r'^(?P<id>[0-9]*)$',
                           RedirectView.as_view(url='/view/%(id)s')),
                       url(r'^view/(?P<id>[0-9]*)$',
                           view=bt.ViolationView.as_view(),
                           name="violation_url"),
                       url(r'^attach/(?P<id>[0-9]*)$',
                           view=bt.get_attach,
                           name="attach"),
                       # different data outputs
                       url(r'^csv$',
                           view=bt.ViolationCsvList.as_view(),
                           name="csv_output"),
                       url(r'^ods$',
                           view=bt.asods,
                           name="ods_output"),
                       url(r'^rss/$',
                           view=RssSiteNewsFeed(),
                           name="rss_output"),
                       url(r'^atom/$',
                           view=AtomSiteNewsFeed(),
                           name="atom_output"),
                       url(r'^activate/$',
                           view=bt.activate,
                           name="activate"),
                       url(r'^confirm/(?P<id>[0-9a-z]*)$',
                           view=bt.confirm,
                           name="confirm"),
                       url(r'^confirm/(?P<id>[0-9]*)/(?P<name>.*)$',
                           view=bt.confirm,
                           name="confirm_full"),
                       url(r'^moderate/$',
                           view=bt.moderate,
                           name="moderate"),
                       url(r'^lookup/',
                           view=bt.LookupView.as_view(template_name='search/lookup.json'),
                           name="lookup"),
                       url(r'^accounts/logout$',
                           'django.contrib.auth.views.logout', {'next_page': '/'}),
                       url(r'^accounts/',
                           include('registration.backends.hmac.urls')),
                       url(r'^comments/',
                           include('django_comments.urls')),
                       url(r'^about/$',
                           TemplateView.as_view(template_name='nn.html')),
                       url(r'^start/$',
                           TemplateView.as_view(template_name='start.html')),
                       url(r'^captcha/',
                           include('captcha.urls')),
                       url(r'^admin/',
                           include(admin.site.urls)),
                       url(r'^api/',
                           include(api_resource.urls)),
                       url(r'^api/',
                           include(operator_api_resource.urls)),
                       url(r'^search/',
                           bt.ViolationSearchView.as_view(template_name='search/search.html')),
                       # Language switch
                       url(r'^i18n/', include('django.conf.urls.i18n')),
                       )

if settings.DEV_SERVER is True:
    urlpatterns += patterns('',
                            (r'^site_media/(?P<path>.*)$',
                             'django.views.static.serve',
                             {'document_root': settings.MEDIA_PATH}),
                            )
