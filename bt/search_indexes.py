from haystack import indexes
from models import Violation


class ViolationIndexes(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    operator = indexes.CharField(model_attr="operator_ref")
    country = indexes.CharField(model_attr="country")
    contract = indexes.CharField(model_attr="contract")
    type = indexes.CharField(model_attr="contractual")
    media = indexes.CharField(model_attr="media")
    operator_name = indexes.CharField()
    state = indexes.NgramField(model_attr="state")
    old = indexes.BooleanField(model_attr="old")

    def get_model(self):
        return Violation

    def prepare_operator_name(self, obj):
        return obj.operator_ref.name
