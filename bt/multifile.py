"""
A newforms widget and field to allow multiple file uploads.

Created by Edward Dale (www.scompt.com)
Released into the Public Domain
"""

from django.utils.datastructures import MultiValueDict, MergeDict
from django.forms.fields import Field, EMPTY_VALUES
from django.forms.widgets import Input, FILE_INPUT_CONTRADICTION
from django.forms.util import ValidationError

FILE_INPUT_EMPTY_VALUE = object()


class MultiFileInput(Input):
    """
    A widget to be used by the MultiFileField to allow the user to upload
    multiple files at one time.
    """
    input_type = 'file'
    needs_multipart_form = True

    def render(self, name, value, attrs=None):
        """
        Renders the MultiFileInput.
        Should not be overridden.  Instead, subclasses should override the
        js, link, and/or fields methods which provide content to this method.
        """
        if attrs is None:
            attrs = {}

        name += '[]'
        attrs['multiple'] = 'multiple'

        return super(MultiFileInput, self).render(name, None, attrs=attrs)

    def value_from_datadict(self, data, files, name):
        """
        File widget takes data from FILES, not POST
        we need to add [] for w3c recoomendation
        """
        name += '[]'
        if isinstance(files, (MultiValueDict, MergeDict)):
            return files.getlist(name)
        return files.get(name, None)

    def id_for_label(self, id_):
        """
        The first file input box always has a 0 appended to it's id.
        """
        if id_:
            id_ += '0'
        return id_
    id_for_label = classmethod(id_for_label)


class MultiFileField(Field):
    """
    A field allowing users to upload multiple files at once.
    """
    widget = MultiFileInput
    max_length = None
    allow_empty_file = None

    def to_python(self, data):
        """
        Cleans the data and makes sure that all the files had some content.
        Also checks whether a file was required.
        """
        if data in EMPTY_VALUES:
            return None
        if data is FILE_INPUT_EMPTY_VALUE:
            raise validationError(self.Error_messages['empty_multiply'])

        # UploadedFile objects should have name and size attributes
        for d in data:
            try:
                file_name = d.name
                file_size = d.size
            except AttributeError:
                raise ValidationError(self.Error_messages['invalid'])

            if self.max_length is not None and file_size > self.max_length:
                error_values = {'max': self.max_length, 'length': file_size}
                raise ValidationError(self.error_messages['max_length'] % error_values)
            if not file_name:
                raise ValidationError(self.error_messages['invalid'])
            if not self.allow_empty_file and not file_size:
                raise ValidationError(self.Error_messages['empty'])
        return data

    def bound_data(self, data, initial):
        if data in (None, FILE_INPUT_EMPTY_VALUE, FILE_INPUT_CONTRADICTION):
            return initial
        return data
