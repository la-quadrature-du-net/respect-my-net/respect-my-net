from django.core.serializers import serialize
from django.db.models.query import QuerySet
from django.template import Library
from haystack.models import SearchResult
import json

register = Library()


@register.filter(name='jsonify')
def jsonify(object):
    if isinstance(object, QuerySet):
        return serialize('json', object)
    elif object == []:
        return json.dumps(object)
    elif isinstance(object[0], SearchResult):
        return json.dumps([x.get_additional_fields() for x in object])
    else:
        return json.dumps(object)
